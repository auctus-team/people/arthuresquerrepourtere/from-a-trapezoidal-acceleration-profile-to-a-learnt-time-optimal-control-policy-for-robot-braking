import gymSimu as gs
from stable_baselines3 import SAC, TD3, DDPG, PPO, A2C
from stable_baselines3.common.callbacks import EvalCallback
import pickle
import zipfile
import tools
import os
import numpy as np

import sys
import matplotlib.pyplot as plt


def launchLearning_old(algo,robotName,path,rewardShaping,delta,timesteps,policy_kwargs,learning_rate, discrete=None, deltaRange=None,curriculum=None,excitingTrajectories=True,PCPAT=None,dynamicDelta=False,deltaOnDOFs=None):
    
    env=gs.GymSimu(robotName,rewardShaping=rewardShaping,delta=delta,curriculum=curriculum,discrete=discrete, deltaRange=deltaRange,excitingTrajectories=excitingTrajectories,PCPAT=PCPAT,dynamicDelta=dynamicDelta,deltaOnDOFs=deltaOnDOFs)
    eval_env = gs.GymSimu(robotName,rewardShaping=rewardShaping,delta=delta,discrete=discrete, deltaRange=deltaRange, evaluator=True,excitingTrajectories=excitingTrajectories,PCPAT=PCPAT,dynamicDelta=dynamicDelta,deltaOnDOFs=deltaOnDOFs)
    
    if algo=="SAC":
        model = SAC('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate,learning_starts=100000, train_freq=50, target_update_interval=50)
    elif algo=="TD3":
        model = TD3('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate,learning_starts=100000, train_freq=50)
    elif algo=="DDPG":
        model = DDPG('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate,learning_starts=100000, train_freq=50)
    elif algo=="PPO":
        model = PPO('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate)
    elif algo=="A2C":
        model = A2C('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate)
    eval_callback = EvalCallback(eval_env, best_model_save_path=path,n_eval_episodes=49,
                             log_path=path, eval_freq=50000,
                             deterministic=True, render=False)
    model.learn(total_timesteps=timesteps, callback=eval_callback)
    with open(path+"lenghtsEval.txt", "wb") as fp:
        pickle.dump(eval_env.lengths, fp)
    with open(path+"stoppedEval.txt", "wb") as fp:
        pickle.dump(eval_env.stopped, fp)
    with zipfile.ZipFile(path+'/evaluations.npz', 'r') as zip_ref:
        zip_ref.extractall(path+'/evaluations')
    model.save(path+"final_model")
    

def launchLearning(algo,robotName,path,rewardShaping,delta,timesteps,policy_kwargs,learning_rate, discrete=None, deltaRange=None,curriculum=None,excitingTrajectories=True,PCPAT=None,dynamicDelta=False,deltaOnDOFs=None,nbTrial=None):
    if nbTrial is None:
        env=gs.GymSimu(robotName,rewardShaping=rewardShaping,delta=delta,curriculum=curriculum,discrete=discrete, deltaRange=deltaRange,excitingTrajectories=excitingTrajectories,PCPAT=PCPAT,dynamicDelta=dynamicDelta,deltaOnDOFs=deltaOnDOFs)
        eval_env = gs.GymSimu(robotName,rewardShaping=rewardShaping,delta=delta,discrete=discrete, deltaRange=deltaRange, evaluator=True,excitingTrajectories=excitingTrajectories,PCPAT=PCPAT,dynamicDelta=dynamicDelta,deltaOnDOFs=deltaOnDOFs)
        
        if algo=="SAC":
            model = SAC('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate,learning_starts=100000, train_freq=50, target_update_interval=50)
        elif algo=="TD3":
            model = TD3('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate,learning_starts=100000, train_freq=50)
        elif algo=="DDPG":
            model = DDPG('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate,learning_starts=100000, train_freq=50)
        elif algo=="PPO":
            model = PPO('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate)
        elif algo=="A2C":
            model = A2C('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate)
        eval_callback = EvalCallback(eval_env, best_model_save_path=path,n_eval_episodes=49,
                                 log_path=path, eval_freq=50000,
                                 deterministic=True, render=False)
        model.learn(total_timesteps=timesteps, callback=eval_callback)
        with open(path+"lenghtsEval.txt", "wb") as fp:
            pickle.dump(eval_env.lengths, fp)
        with open(path+"stoppedEval.txt", "wb") as fp:
            pickle.dump(eval_env.stopped, fp)
        with zipfile.ZipFile(path+'/evaluations.npz', 'r') as zip_ref:
            zip_ref.extractall(path+'/evaluations')
        model.save(path+"final_model")
    else:
        rangeEnergy=[0,0.2,0.4,np.inf]
        #rangeEnergy=[0,np.inf]
        
        testSet=np.loadtxt("./testSets/"+robotName+"TestSetFinal.csv", delimiter=",")
        
        kineticEnergies=np.zeros(testSet.shape[0])
        
        env=gs.GymSimu(robotName,rewardShaping=rewardShaping,delta=delta,curriculum=curriculum,discrete=discrete, deltaRange=deltaRange,excitingTrajectories=excitingTrajectories,PCPAT=PCPAT,dynamicDelta=dynamicDelta,deltaOnDOFs=deltaOnDOFs)

        for i in range(testSet.shape[0]):
            obs=np.reshape(testSet[i],(3,env.simulator.DOFs))
            obs=env.resetAtState(obs[0],obs[1],obs[2])
            kineticEnergies[i]=env.simulator.computeKineticEnergy()


        trap_env = gs.GymSimu(robotName,rewardShaping=rewardShaping,delta=False, evaluator=False,excitingTrajectories=excitingTrajectories)
        evaTrapEXT=tools.evaluator(trap_env,testSet=testSet)
        evaTrapEXT.evaluateTrap()
        
        stepsTrap=np.array(evaTrapEXT.stepNumbersTrap)
        energyTrap=np.array(evaTrapEXT.energiesTrap)
        
        reward=np.zeros(nbTrial)
        steps=np.zeros(nbTrial)
        energy=np.zeros(nbTrial)
        stepsAll=np.zeros((nbTrial,testSet.shape[0]))
        gainsSteps=np.zeros((nbTrial,testSet.shape[0]))
        energyAll=np.zeros((nbTrial,testSet.shape[0]))
        gainsEnergy=np.zeros((nbTrial,testSet.shape[0]))
        
        for i in range(nbTrial):
            
            print("TRIAL : "+str(i))
            
            pathTrial=path[:-1]+"_trial_"+str(i)+"/"
            
            env=gs.GymSimu(robotName,rewardShaping=rewardShaping,delta=delta,curriculum=curriculum,discrete=discrete, deltaRange=deltaRange,excitingTrajectories=excitingTrajectories,PCPAT=PCPAT,dynamicDelta=dynamicDelta,deltaOnDOFs=deltaOnDOFs)
            
            if not os.path.isfile(pathTrial+"final_model.zip"):
                
                eval_env = gs.GymSimu(robotName,rewardShaping=rewardShaping,delta=delta,discrete=discrete, deltaRange=deltaRange, evaluator=True,excitingTrajectories=excitingTrajectories,PCPAT=PCPAT,dynamicDelta=dynamicDelta,deltaOnDOFs=deltaOnDOFs)
                
                if algo=="SAC":
                    model = SAC('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate,learning_starts=100000, train_freq=50, target_update_interval=50)
                elif algo=="TD3":
                    model = TD3('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate,learning_starts=100000, train_freq=50)
                elif algo=="DDPG":
                    model = DDPG('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate,learning_starts=100000, train_freq=50)
                elif algo=="PPO":
                    model = PPO('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate)
                elif algo=="A2C":
                    model = A2C('MlpPolicy', env, policy_kwargs=policy_kwargs,verbose=1, learning_rate=learning_rate)
                eval_callback = EvalCallback(eval_env, best_model_save_path=pathTrial,n_eval_episodes=49,
                                         log_path=pathTrial, eval_freq=50000,
                                         deterministic=True, render=False)
                model.learn(total_timesteps=timesteps, callback=eval_callback)
                with open(pathTrial+"lenghtsEval.txt", "wb") as fp:
                    pickle.dump(eval_env.lengths, fp)
                with open(pathTrial+"stoppedEval.txt", "wb") as fp:
                    pickle.dump(eval_env.stopped, fp)
                with zipfile.ZipFile(pathTrial+'/evaluations.npz', 'r') as zip_ref:
                    zip_ref.extractall(pathTrial+'/evaluations')
                model.save(pathTrial+"final_model")

            model=tools.loadModel(algo,pathTrial+"best_model")
            
            evaPolicyExt=tools.evaluator(env,model=model,testSet=testSet)
            evaPolicyExt.evaluatePolicy()
            reward[i]=np.mean(np.array(evaPolicyExt.logs["rew"]))
            steps[i]=np.mean(np.array(evaPolicyExt.logs["step"]))
            energy[i]=np.mean(np.array(evaPolicyExt.logs["energy"]))
            stepsAll[i]=np.array(evaPolicyExt.logs["step"])
            gainsSteps[i]=1-(stepsAll[i]/stepsTrap)
            energyAll[i]=np.array(evaPolicyExt.logs["energy"])
            gainsEnergy[i]=1-(energyAll[i]/energyTrap)
            
            print("Number worst braking time : ",np.count_nonzero(np.array(evaPolicyExt.logs["step"][0])>np.array(evaTrapEXT.stepNumbersTrap)))
            longer=np.array(evaPolicyExt.logs["step"][0])>np.array(evaTrapEXT.stepNumbersTrap)
            print("time loss : ",np.array(evaPolicyExt.logs["step"][0])[longer]-np.array(evaTrapEXT.stepNumbersTrap)[longer])
            print("MAX time loss : ",np.max(np.array(evaPolicyExt.logs["step"][0])[longer]-np.array(evaTrapEXT.stepNumbersTrap)[longer]))
        print("##############")
        print("TAP")
        for i in range(len(rangeEnergy)-1):
            print()
            print("------------------")
            print("Mean step between "+str(rangeEnergy[i])+" and "+str(rangeEnergy[i+1])+" : "+
                                      str(np.mean(stepsTrap[np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])])))
            print("Mean energy between "+str(rangeEnergy[i])+" and "+str(rangeEnergy[i+1])+" : "+
                                      str(np.mean(energyTrap[np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])])))
        print()
        print("##############")
        print("LEARNING SYSTEM")
        for i in range(len(rangeEnergy)-1):
            print()
            print("-------Step-------")
            print("Mean step between "+str(rangeEnergy[i])+" and "+str(rangeEnergy[i+1])+" : "+
                                      str(np.mean(stepsAll[:,np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])])))
            print("Std step between "+str(rangeEnergy[i])+" and "+str(rangeEnergy[i+1])+" : "+
                                      str(np.std(np.mean(stepsAll[:,np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])],axis=1))))
            print("Gain step between "+str(rangeEnergy[i])+" and "+str(rangeEnergy[i+1])+" : "+
                                      str(np.mean(gainsSteps[:,np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])])))
            print("Gain std step between "+str(rangeEnergy[i])+" and "+str(rangeEnergy[i+1])+" : "+
                                      str(np.std(np.mean(gainsSteps[:,np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])],axis=1))))
            print(np.mean(stepsAll[:,np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])],axis=1))
            print(np.mean(stepsAll[:,np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])],axis=1)<np.mean(stepsTrap[np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])]))
            print(np.count_nonzero(np.mean(stepsAll[:,np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])],axis=1)<np.mean(stepsTrap[np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])])))
            
            print("-------Energy-------")
            print("Mean energy between "+str(rangeEnergy[i])+" and "+str(rangeEnergy[i+1])+" : "+
                                      str(np.mean(energyAll[:,np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])])))
            print("Std energy between "+str(rangeEnergy[i])+" and "+str(rangeEnergy[i+1])+" : "+
                                      str(np.std(np.mean(energyAll[:,np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])],axis=1))))
            print("Gain energy between "+str(rangeEnergy[i])+" and "+str(rangeEnergy[i+1])+" : "+
                                      str(np.mean(gainsEnergy[:,np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])])))
            print("Gain std energy between "+str(rangeEnergy[i])+" and "+str(rangeEnergy[i+1])+" : "+
                                      str(np.std(np.mean(gainsEnergy[:,np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])],axis=1))))
            print(np.mean(energyAll[:,np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])],axis=1))
            print(np.mean(energyAll[:,np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])],axis=1)<np.mean(energyTrap[np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])]))
            print(np.count_nonzero(np.mean(energyAll[:,np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])],axis=1)<np.mean(energyTrap[np.logical_and(rangeEnergy[i] <= kineticEnergies,kineticEnergies < rangeEnergy[i+1])])))
         
        

        #sys.exit()
        print()
        print()
        print("______________________")
        print("ALL ENERGIES COMBINED")
        print("Reward")
        print(reward)
        print(np.mean(reward))
        print(np.std(reward))
        print("####")
        print("Step")
        print(steps)
        print(np.mean(steps))
        print(np.std(steps))
        print("Energy")
        print(energy)
        print(np.mean(energy))
        print(np.std(energy))
        
        
        
        
        
        
        
        
        
        